

package app.firmador.gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import javax.imageio.ImageIO;
import java.io.File;
import javax.swing.JComponent;
import java.awt.BasicStroke;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import net.coobird.thumbnailator.*;

public class DrawArea extends JComponent {
 
  // Image in which we're going to draw
  private BufferedImage image;
  // Graphics2D object ==> used to draw on
  private Graphics2D g2;
  // Mouse coordinates
  private int currentX, currentY, oldX, oldY;
  
  private boolean enabled;
    
  public DrawArea() {
    setDoubleBuffered(false);
    addMouseListener(new MouseAdapter() {
      public void mousePressed(MouseEvent e) {
        // save coord x,y when mouse is pressed
        oldX = e.getX();
        oldY = e.getY();
      }
    });
 
    addMouseMotionListener(new MouseMotionAdapter() {
      public void mouseDragged(MouseEvent e) {
        // coord x,y when drag mouse
        currentX = e.getX();
        currentY = e.getY();
 
        if (g2 != null && enabled) {
          // draw line if g2 context not null
          g2.drawLine(oldX, oldY, currentX, currentY);
          // refresh draw area to repaint
          repaint();
          // store current coords x,y as olds x,y
          oldX = currentX;
          oldY = currentY;
        }
      }
    });
  }
 
  protected void paintComponent(Graphics g) {
    if (image == null) {
      // image to draw null ==> we create
      image = new BufferedImage(460,150,BufferedImage.TYPE_INT_ARGB);
      g2 = (Graphics2D) image.getGraphics();
      // enable antialiasing
      g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
      // clear draw area
      clear();
    }
 
    g.drawImage(image, 0, 0, null);
  }
 
  // now we create exposed methods
  public void clear() {
    g2.setPaint(Color.white);
    // draw white on entire draw area to clear
    g2.fillRect(0, 0, 460,150);
    g2.setPaint(Color.black);
    repaint();
  }
 
  
  public void black() {
    g2.setPaint(Color.black);
  }
 

  public void blue() {
    g2.setPaint(Color.blue);
  }

  public void thin() {
    // apply red color on g2 context
    g2.setStroke(new BasicStroke(1,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND));
  }
 
  public void normal() {
    g2.setStroke(new BasicStroke(3,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND));
  }
 
  public void thick() {
    g2.setStroke(new BasicStroke(5,BasicStroke.CAP_ROUND,BasicStroke.JOIN_ROUND));
  }
 
 
  public void save(String pathToSavedImages){
     try{
         ImageIO.write(image,"png",new File(pathToSavedImages));
      }catch(Exception e){
         
     }
  }
  
  public void setEnable(boolean enable){
      this.enabled = enable;
  }      
}