package app.firmador.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Image;
import java.awt.Window;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.awt.Insets;
import java.awt.font.TextAttribute;
import java.util.Map;
import java.util.HashMap;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Paths;
import java.security.KeyStore.PasswordProtection;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListSelectionModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JToggleButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.TransferHandler;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.BorderFactory;
import javax.swing.JComboBox;
import java.awt.Rectangle;
import javax.swing.ImageIcon;
import javax.swing.Icon;
import app.firmador.FirmadorPAdES;
import app.firmador.FirmadorXAdES;
import app.firmador.FirmadorOpenDocument;
import app.firmador.Report;
import app.firmador.Validator;
import app.firmador.gui.swing.CopyableJLabel;
import app.firmador.gui.swing.ScrollableJPanel;
import com.apple.eawt.Application;
import com.google.common.base.Throwables;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.FileDocument;
import eu.europa.esig.dss.model.MimeType;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle; 
import net.coobird.thumbnailator.*;
import javax.imageio.ImageIO;







public class GUISign{
    protected GUIInterface gui;
    //final JFrame signFrame;
    private JTextField fileField;
    private JButton loadImageB;
    private File fileLoad;
    private String signPath = "";
    private String lastImagePath;
    private String imagePath;
    private JComboBox sizes;
    private JComboBox formatCB;
    private JCheckBox createSignCB;

    private  JSpinner pageSpinner;
    private JLabel signatureLabel;
    private JTextField pNameT;
    private JTextField pIdentificationT;
    private  JTextField pReasonT;
    private  JTextField pPlaceT;
    private  JTextField pContactT;
    private JTextArea pFreeTextT;
    private  JCheckBox FreeTextCB;
    private  JCheckBox signatureVisibleCheckBox;
    private JCheckBox NotDefaultCB;
    private JToggleButton boldB;
    private JToggleButton italicsB;
 

    public GUISign(File fileLoad, GUIInterface gui){
        this.fileLoad = fileLoad;
        this.gui = gui;
    } 

    public JPanel createWhiteBoard(){
        final boolean state = false;
        JPanel whiteBP = new JPanel();
        createSignCB = new JCheckBox("Crear Firma", false);
        JButton blackB  = new JButton();
        JButton blueB   = new JButton();
        JButton thinB   = new JButton(); 
        JButton normalB = new JButton();
        JButton thickB  = new JButton();  
        JButton clearB  = new JButton();
        JButton doneB   = new JButton();

        signPath = System.getProperty("user.home") + "/signature.png";
        try{
            Image blackDotI = ImageIO.read(this.getClass().getClassLoader()
            .getResource("black.png"));

            blackB.setIcon(new ImageIcon(blackDotI));
            
            Image blueDotI = ImageIO.read(this.getClass().getClassLoader()
            .getResource("blue.png"));
            blueB.setIcon(new ImageIcon(blueDotI));

            Image thinI = ImageIO.read(this.getClass().getClassLoader()
            .getResource("thin.png"));
            thinB.setIcon(new ImageIcon(thinI));

            Image normalI = ImageIO.read(this.getClass().getClassLoader()
            .getResource("normal.png"));
            normalB.setIcon(new ImageIcon(normalI));

            Image thickI = ImageIO.read(this.getClass().getClassLoader()
            .getResource("thick.png"));
            thickB.setIcon(new ImageIcon(thickI));

            Image doneI = ImageIO.read(this.getClass().getClassLoader()
            .getResource("done.png"));
            doneB.setIcon(new ImageIcon(doneI));

            Image clearI = ImageIO.read(this.getClass().getClassLoader()
            .getResource("close.png"));
            clearB.setIcon(new ImageIcon(clearI));
        }catch (Exception ex) {
            System.out.println(ex);
          }
        
        DrawArea whiteB = new DrawArea();
        whiteB.setEnable(state);
        blackB.setEnabled(state);
        blueB.setEnabled(state);
        thinB.setEnabled(state);
        normalB.setEnabled(state);
        thickB.setEnabled(state);
        clearB.setEnabled(state);
        doneB.setEnabled(state);
        
        ItemListener checkBListener = new ItemListener(){
            public void itemStateChanged(ItemEvent e){
                boolean state2 = createSignCB.isSelected();
                if (!state2){
                    whiteB.clear();
                    loadImageB.setEnabled(true);
                    fileField.setEnabled(true);
                }else{
                    loadImageB.setEnabled(false);
                    fileField.setEnabled(false);
                }
                whiteB.setEnable(state2);
                blackB.setEnabled(state2);
                blueB.setEnabled(state2);
                thinB.setEnabled(state2);
                normalB.setEnabled(state2);
                thickB.setEnabled(state2);
                clearB.setEnabled(state2);
                doneB.setEnabled(state2);
            }
        };
                
        ActionListener actionListener = new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (e.getSource() == clearB) {
                    whiteB.clear();
                } else if (e.getSource() == blackB) {
                    whiteB.black();
                } else if (e.getSource() == blueB) {
                    whiteB.blue();
                } else if (e.getSource() == thinB) {
                    whiteB.thin();
                } else if (e.getSource() == normalB) {
                    whiteB.normal();
                } else if (e.getSource() == thickB) {
                    whiteB.thick();
                }else if (e.getSource() == doneB) {
                    whiteB.save(signPath);
                    signatureLabel.setIcon(new ImageIcon(resizeImage(signPath, 45, 45)));    
                }
            }
        };
        createSignCB.addItemListener(checkBListener);
        clearB.addActionListener(actionListener);
        blackB.addActionListener(actionListener);
        blueB.addActionListener(actionListener);
        thinB.addActionListener(actionListener);
        normalB.addActionListener(actionListener);
        thickB.addActionListener(actionListener);
        doneB.addActionListener(actionListener);
        
        JPanel ButtonP = new JPanel();
        BoxLayout layout1 = new BoxLayout(ButtonP,BoxLayout.Y_AXIS);
        ButtonP.setLayout(layout1);
        
        ButtonP.add(blackB);
        ButtonP.add(blueB);
        ButtonP.add(thinB);
        ButtonP.add(normalB);
        ButtonP.add(thickB);
        ButtonP.add(clearB);
        ButtonP.add(doneB);
        
        BoxLayout layout2 = new BoxLayout(whiteBP,BoxLayout.X_AXIS);
        whiteBP.setLayout(layout2);
        whiteBP.add(whiteB);
        whiteBP.add(ButtonP);
        
        JPanel finalP = new JPanel();
        JPanel checkBP = new JPanel(new FlowLayout(FlowLayout.LEFT));
        checkBP.add(createSignCB);
        finalP.setBorder(BorderFactory.createTitledBorder("Dibujar Firma"));
        BoxLayout layout3 = new BoxLayout(finalP,BoxLayout.Y_AXIS);
        finalP.setLayout(layout3);
        
        
        finalP.add(checkBP);
        finalP.add(whiteBP,FlowLayout.CENTER);
        return finalP;
    }
    
    public  JPanel createLocation(JFrame signFrame){
        
        JPanel locationP = new JPanel();
        JPanel imageP = new JPanel();
        JPanel parametersP = new JPanel();
        JPanel positionP = new JPanel();
        JPanel fileP = new JPanel();
        
        positionP.setBorder(BorderFactory.createTitledBorder("Posición"));
        fileP.setBorder(BorderFactory.createTitledBorder("Imagen Personal"));
        
        BoxLayout layout1 = new BoxLayout(positionP,BoxLayout.Y_AXIS);
        BoxLayout layout2 = new BoxLayout(parametersP, BoxLayout.Y_AXIS);
        BoxLayout layout3 = new BoxLayout(locationP,BoxLayout.X_AXIS);
        
        positionP.setLayout(layout1);
        parametersP.setLayout(layout2);
        
        JPanel pageP = new JPanel();
        JLabel page = new JLabel("Página:");    
        JLabel xCoordinateL = new JLabel("Coordenada X:");
        Dimension d2 = xCoordinateL.getPreferredSize();
        d2.width = 140;
        page.setPreferredSize(d2);
        xCoordinateL.setPreferredSize(d2);
        pageSpinner = new JSpinner(new SpinnerNumberModel());

        Dimension d = pageSpinner.getPreferredSize();
        d.width = 220;
        pageSpinner.setPreferredSize(d);
        pageP.add(page);
        pageP.add(pageSpinner);   

        JPanel xcoordP = new JPanel();
        JSpinner xCoordinate = new JSpinner(new SpinnerNumberModel(0,0,1000,1));
        xCoordinate.setPreferredSize(d);;
        xcoordP.add(xCoordinateL);
        xcoordP.add(xCoordinate);

        JPanel ycoordP = new JPanel();
        JLabel yCoordinateL = new JLabel("Coordenada Y:");
        yCoordinateL.setPreferredSize(d2);
        JSpinner yCoordinate = new JSpinner(new SpinnerNumberModel(0,0,1000,1));
        yCoordinate.setPreferredSize(d);
        ycoordP.add(yCoordinateL);
        ycoordP.add(yCoordinate);
        
        JPanel sizeP = new JPanel();
        JLabel size = new JLabel("Tamaño:");
        size.setPreferredSize(d2);
        String[] sizesStrings = {"Muy pequeño","Pequeño","Normal","Grande","Muy grande"};
        sizes = new JComboBox(sizesStrings);
        sizes.setBackground(Color.white);
        Dimension d3 = sizes.getPreferredSize();
        d3.width = 220;
        sizes.setPreferredSize(d3);
        sizes.setSelectedIndex(2);
        sizes.addActionListener( new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                int actWidth = 90;
                int actHeight = 30;
                int newWidth = 0;
                int newHeight = 0;
                switch (sizes.getSelectedIndex()){
                    case 0: newWidth = (int)Math.round(0.70 * actWidth);
                            newHeight = (int)Math.round(0.70 * actHeight);
                            break;

                    case 1: newWidth = (int)Math.round(0.85 * actWidth);
                    newHeight = (int)Math.round(0.85 * actHeight);
                    break;


                    case 2: newWidth = (int)Math.round(1 * actWidth);
                    newHeight = (int)Math.round(1 * actHeight);
                    break;


                    case 3: newWidth = (int)Math.round(1.15 * actWidth);
                            newHeight = (int)Math.round(1.15 * actHeight);
                            break;


                    case 4: newWidth = (int)Math.round(1.30 * actWidth);
                            newHeight = (int)Math.round(1.30 * actHeight);
                            break;
                }
                signatureLabel.setSize(newWidth, newHeight);
            }
        }
        );
        sizeP.add(size);
        sizeP.add(sizes);
        
        JPanel formatP = new JPanel();
        JLabel formatL = new JLabel("Formato: ");
        formatL.setPreferredSize(d2);
        String[] formatS = {"Imagen y firma (horizontal)", "Solo imagen", "Solo firma"};
        formatCB = new JComboBox(formatS);
        Dimension d4 = formatCB.getPreferredSize();
        d4.width = 220;
        formatCB.setPreferredSize(d3);
        formatCB.setBackground(Color.white);
        formatCB.setSelectedIndex(2);
        formatP.add(formatL);
        formatP.add(formatCB);
        
        positionP.add(pageP);
        positionP.add(xcoordP);
        positionP.add(ycoordP);
        positionP.add(sizeP);
        positionP.add(formatP);
        
        fileField = new JTextField("",40);
        fileField.setText(lastImagePath);
        fileField.setEditable(false);
        fileField.setBackground(Color.WHITE);
        loadImageB = new JButton();
        try{
            Image loadImageIcon = ImageIO.read(this.getClass().getClassLoader()
            .getResource("AddPicture.png"));
            loadImageB.setIcon(new ImageIcon(loadImageIcon));
        }catch (Exception ex) {
            System.out.println(ex);
        }
        loadImageB.setMargin(new Insets(0,0,0,0));
        loadImageB.setBorder(null);
        loadImageB.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event) {
                FileDialog loadDialog = new FileDialog(signFrame,"Seleccionar imagen para adjuntar a la firma");
                loadDialog.setMultipleMode(false);
                loadDialog.setFilenameFilter(new FilenameFilter() {
                    public boolean accept(File dir, String name) {
                        return name.toLowerCase().endsWith(".jpeg")
                        || name.toLowerCase().endsWith(".jpg")
                        || name.toLowerCase().endsWith(".gif")
                        || name.toLowerCase().endsWith(".tiff")
                        || name.toLowerCase().endsWith(".tif")
                        || name.toLowerCase().endsWith(".png");
                    }
                });
                loadDialog.setFile("*.jpeg;*.jpg;*.gif;*.tiff;*.tif;*.png");
                loadDialog.setLocationRelativeTo(null);
                loadDialog.setVisible(true);
                loadDialog.dispose();
                if (loadDialog.getFile() != null) {
                        String path = loadDialog.getDirectory() + loadDialog.getFile();
                        signatureLabel.setIcon(new ImageIcon(resizeImage(path, 32, 32)));
                        if(formatCB.getSelectedIndex() == 1){
                            signatureLabel.setHorizontalAlignment(JLabel.CENTER);
                        }
                        fileField.setText(path); 
                        imagePath = path;
                        System.out.printf("imagePath in SignGui: %s \n", imagePath);
                        lastImagePath = path;
                        System.out.printf("lastImagePath in SignGui: %s \n", lastImagePath);
                }
        }
    });
        fileP.add(fileField);
        fileP.add(loadImageB);
        
        
        File file;
        PDDocument document;
        PDFRenderer renderer;
        JLabel imageLabel = new JLabel();
        BufferedImage pageImage;
        
        try{
            document = PDDocument.load(fileLoad);
            int pages = document.getNumberOfPages();
            renderer = new PDFRenderer(document);
            pageImage = renderer.renderImage(0,1/1.25f);
            SpinnerNumberModel model =  ((SpinnerNumberModel)pageSpinner.getModel());
            model.setMinimum(0);
            model.setMaximum(pages);
            pageSpinner.addChangeListener(new spinnerListener(pageSpinner,pageImage,renderer,imageLabel,gui));
            
            
            imageLabel.setBorder(new LineBorder(Color.BLACK));
            imageLabel.setIcon(new ImageIcon(pageImage));
            
        }catch (Exception ex) {
            ex.printStackTrace();
            gui.showError(Throwables.getRootCause(ex));
        }
        
        ImageIcon imgIconLabel = new ImageIcon(this.getClass().getClassLoader()
        .getResource("picture.png"));
        
        signatureLabel =
            new JLabel("Firma visible");
        signatureLabel.setCursor(
            Cursor.getPredefinedCursor(Cursor.MOVE_CURSOR));
        signatureLabel.setForeground(new Color(0, 0, 0, 0));
        signatureLabel.setBackground(new Color(127, 127, 127, 127));
        signatureLabel.setOpaque(true);
        signatureLabel.setBounds(new Rectangle(90,30));
        
        imageLabel.addMouseMotionListener(new MouseMotionAdapter() {
            public void mouseDragged(MouseEvent e) {
                int xpos = e.getX() - signatureLabel.getWidth() / 2 ;
                int ypos = e.getY() - signatureLabel.getHeight() / 2;
                xCoordinate.setValue(xpos);
                yCoordinate.setValue(ypos);
                signatureLabel.setLocation((int)xpos,(int)ypos);
            }
        });
        xCoordinate.addChangeListener(new spinnerListenerCoord(xCoordinate,yCoordinate,signatureLabel));
        yCoordinate.addChangeListener(new spinnerListenerCoord(xCoordinate,yCoordinate,signatureLabel));
        
        JPanel signVis = new JPanel();
        signatureVisibleCheckBox = new JCheckBox("Firma no visible");
        signVis.add(signatureVisibleCheckBox);

        JPanel whiteB = createWhiteBoard();
        imageLabel.add(signatureLabel);
        imageP.add(imageLabel);
        parametersP.add(positionP);
        parametersP.add(fileP);
        parametersP.add(whiteB);
        parametersP.add(signVis);
        
        locationP.add(imageP);
        locationP.add(parametersP);

        return locationP;
    }
    

    
    public  JPanel createProperties(){
        JPanel propertiesP = new JPanel();
        JPanel propertiesP2 = new JPanel();

        JPanel propertiesMain = new JPanel();
        propertiesMain.setLayout(new BorderLayout());
        BoxLayout layout1 = new BoxLayout(propertiesP,BoxLayout.Y_AXIS);
        BoxLayout layout2 = new BoxLayout(propertiesP2,BoxLayout.Y_AXIS);

        propertiesP.setLayout(layout1);
        propertiesP2.setLayout(layout2);
        
        JPanel ButtonsPane = new JPanel(new FlowLayout(FlowLayout.LEFT));
        JPanel NamePane = new JPanel();
        JPanel IdentificationPane = new JPanel();
        JPanel ReasonPane = new JPanel();
        JPanel LocationPane = new JPanel();
        JPanel ContactPane = new JPanel();
        JPanel FreePane = new JPanel();
        JPanel NotDefaultPane = new JPanel();

        boldB = new JToggleButton();
        italicsB = new JToggleButton();

        try{
            Image boldI = ImageIO.read(this.getClass().getClassLoader()
            .getResource("bold.png"));
            boldB.setIcon(new ImageIcon(boldI));

            Image italicsI = ImageIO.read(this.getClass().getClassLoader()
            .getResource("italic.png"));
            italicsB.setIcon(new ImageIcon(italicsI));
        }catch (Exception ex) {
            System.out.println(ex);
        }
        
        boldB.setMargin(new Insets(15,15,15,15));
        boldB.setSize(26, 26);
        boldB.setBorder(new LineBorder(Color.BLACK));
        

        italicsB.setMargin(new Insets(15,15,15,15));
        italicsB.setSize(26, 26);
        italicsB.setBorder(new LineBorder(Color.BLACK));
        
        ButtonsPane.add(boldB);
        ButtonsPane.add(italicsB);
 
        NotDefaultCB = new JCheckBox("No usar texto predeterminado");

        JLabel pName = new JLabel("Nombre:         ");
        JLabel pIdentification = new JLabel("Identificación: ");
        JLabel pReason = new JLabel("Razón:          ");
        JLabel pPlace = new JLabel("Ubicación:      ");
        JLabel pContact = new JLabel("Contacto:       ");
        JLabel pFreeText = new JLabel("Texto Libre:    ");

        pNameT = new JTextField(50);
        pIdentificationT = new JTextField(50);
        pReasonT = new JTextField(50);
        pPlaceT = new JTextField(50);
        pContactT = new JTextField(50);
        pFreeTextT = new JTextArea(10,50);
        
        NamePane.add(pName);
        NamePane.add(pNameT);
        IdentificationPane.add(pIdentification);
        IdentificationPane.add(pIdentificationT);
        ReasonPane.add(pReason);
        ReasonPane.add(pReasonT);
        LocationPane.add(pPlace);
        LocationPane.add(pPlaceT);
        ContactPane.add(pContact);
        ContactPane.add(pContactT);
        FreePane.add(pFreeText);
        FreePane.add(pFreeTextT);
        NotDefaultPane.add(NotDefaultCB);
        
        propertiesP.add(NamePane);
        propertiesP.add(IdentificationPane);
        propertiesP.add(ReasonPane);
        propertiesP.add(LocationPane);
        propertiesP.add(ContactPane);
        propertiesP.add(FreePane);

        propertiesP2.add(NotDefaultPane);
        
        propertiesMain.add(ButtonsPane,BorderLayout.NORTH);
        propertiesMain.add(propertiesP,BorderLayout.CENTER);
        propertiesMain.add(propertiesP2,BorderLayout.SOUTH);
        return propertiesMain;
    }
    
    public  JFrame createSignGUI(){
        final JFrame signFrame = new JFrame();
        signFrame.setDefaultCloseOperation(signFrame.DISPOSE_ON_CLOSE);
        JTabbedPane tabbedPane = new JTabbedPane();
        
        JPanel propertiesP =  createProperties();
        JPanel locationP = createLocation(signFrame);
        
        tabbedPane.addTab("Propiedades", propertiesP);
        tabbedPane.addTab("Ubicación", locationP);
        signFrame.add(tabbedPane,BorderLayout.CENTER);
        
        JPanel Buttons = new JPanel(new FlowLayout(FlowLayout.RIGHT));
        JButton cancel = new JButton("Cancelar");
        JButton firmar = new JButton("Firmar");
        firmar.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event) {
                if(fileLoad != null){
                    Sign();
                }
            }
        });

        cancel.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event) {
                signFrame.dispose();
            }
        });

        Buttons.add(cancel);
        Buttons.add(firmar);
        
        
        signFrame.add(Buttons,BorderLayout.AFTER_LAST_LINE);
        signFrame.pack();
        signFrame.setVisible(true);
        return signFrame;
    }

    public void Sign(){
        DSSDocument toSignDocument = new FileDocument(fileLoad);
        DSSDocument signedDocument = null;
        PasswordProtection pin = getPin();
        if (pin.getPassword() != null
            && pin.getPassword().length != 0) {
            MimeType mimeType = toSignDocument.getMimeType();
            if (mimeType == MimeType.PDF) {
                FirmadorPAdES firmador = new FirmadorPAdES(
                gui);
                firmador.selectSlot();
                if (firmador.selectedSlot == -1) return;
                    firmador.setVisible_signature(!signatureVisibleCheckBox.isSelected());
                    int format = formatCB.getSelectedIndex();
                    int page = (int)pageSpinner.getValue();
                    int x =  (int)Math.round(signatureLabel.getX() * 1.25);
                    int y = (int)Math.round(signatureLabel.getY() * 1.25);
                    int sizeFormat = 100;
                    boolean square = false;
                    double porcentage = 1;

                    switch(sizes.getSelectedIndex()){
                        case 0: porcentage =  0.70;
                                sizeFormat = 70;
                                System.out.printf("Entering Size: %d", sizeFormat);
                                
                                break;
                        
                        case 1: porcentage = 0.85;
                                sizeFormat = 85;
                                System.out.printf("Entering Size: %d", sizeFormat);
                                break;

                        
                        case 2: porcentage = 1;
                                sizeFormat = 100;
                                System.out.printf("Entering Size: %d", sizeFormat);
                                break;
                                
                                
                        case 3: porcentage = 1.15;
                                sizeFormat = 115;
                                System.out.printf("Entering Size: %d", sizeFormat);
                                break;

                        case 4: porcentage = 1.30;
                                sizeFormat = 130;

                                System.out.printf("Entering Size: %d", sizeFormat);
                                break;
                    }

                    System.out.printf("Size: %d", sizeFormat);
        
                    int imageWidht = signatureLabel.getWidth();

                    if (format != 2){
                        if (createSignCB.isSelected()){
                            imagePath = signPath;
                        }
                        if (format == 0){
                            square = getSquare(imagePath);
                            if (square){
                                imageWidht = (int)Math.round( 145 * porcentage);
                            }else{
                                imageWidht = (int)Math.round( 230 * porcentage);
                            }
                            imagePath = resizeImage(imagePath, imageWidht,imageWidht);
                        }else if(format == 1){
                            imageWidht = (int)Math.round( 460 * porcentage);
                            imagePath = resizeImage(imagePath, imageWidht,imageWidht);
                        }    
                    }else{
                        imagePath = null;
                        System.out.println("Setteando image path a null");
                    }

                    String signText = "";
                    
                    if(!pNameT.getText().equals(""))
                    {
                        signText += "Nombre:" + pNameT.getText() + "\n";
                    }
                    if(!pIdentificationT.getText().equals(""))
                    {
                        signText += "Identificación: " + pIdentificationT.getText() + "\n";
                    }
                    if(!pReasonT.getText().equals(""))
                    {
                        signText += "Razón: " + pReasonT.getText() + "\n";
                    }
            
                    if(!pPlaceT.getText().equals(""))
                    {
                        signText += "Ubicación: " + pPlaceT.getText() + "\n";
                    }
            
                    if(!pContactT.getText().equals(""))
                    {
                        signText += "Contacto: " + pContactT.getText() + "\n";
                    }
                    if(!pFreeTextT.getText().equals(""))
                    {
                        signText += pFreeTextT.getText() + "\n";
                    }

                    System.out.printf("ImagePath: %s \n", imagePath);
                    firmador.addVisibleSignature(
                        page+1,
                        x,
                        y,
                        signText,
                        sizeFormat,
                        format,
                        imagePath,
                        square,
                        NotDefaultCB.isSelected(),
                        boldB.isSelected(),
                        italicsB.isSelected()
                    );
                    signedDocument = firmador.sign(toSignDocument,
                        pin);
                } else if (mimeType == MimeType.ODG
                           || mimeType == MimeType.ODP
                           || mimeType == MimeType.ODS
                           || mimeType == MimeType.ODT) {
                                FirmadorOpenDocument firmador =
                                new FirmadorOpenDocument(gui);
                                firmador.selectSlot();
                                if (firmador.selectedSlot == -1) return;
                                signedDocument = firmador.sign(toSignDocument,
                                pin);
                } else {
                    FirmadorXAdES firmador = new FirmadorXAdES(
                        gui);
                    firmador.selectSlot();
                    if (firmador.selectedSlot == -1) return;
                    signedDocument = firmador.sign(toSignDocument,
                        pin);
                }    
                try {
                    pin.destroy();
                }catch (Exception ex) {
                    ex.printStackTrace();
                    gui.showError(Throwables.getRootCause(ex));
                }
                
            }
            if (signedDocument != null) {
                String fileName = getPathToSave();
                if (fileName != null) {
                    try {
                        signedDocument.save(fileName);
                        gui.showMessage(
                            "Documento guardado satisfactoriamente" +
                            " en<br>" + fileName);  
                    } catch (IOException e) {
                        gui.showError(Throwables.getRootCause(e));
                    }
                }
            }
         
    }

    public  boolean needVisualSign(){
        DSSDocument mimeDocument = new FileDocument(fileLoad);
        MimeType mimeType = mimeDocument.getMimeType();
        
        if(mimeType == mimeType.PDF){
            return true;
        }else{
            return false;
        }
    }
    
    public  boolean getSquare(String imagePath){
        boolean square  = false;
        try{
            BufferedImage bimg = ImageIO.read(new File(imagePath));
            double width          =  (double) bimg.getWidth();
            double height         = (double) bimg.getHeight();
            if ((int)Math.round(width/height) == 1){
                square = true;
            }
        }catch(IOException ex){
            gui.showError(Throwables.getRootCause(ex));
        }
        return square;
    }
    
    public  String getPathToSaveR(String suffix) { 
        String lastPath = fileLoad.getAbsolutePath();
        String dotExtension = "";
        int lastDot = lastPath.lastIndexOf(".");
        if (lastDot >= 0) {
            dotExtension = lastPath.substring(lastDot);
        }
        String PathToSave = fileLoad.getPath().substring(0,
        fileLoad.getPath().lastIndexOf(".")) + suffix + dotExtension;
        
        return PathToSave;
    }

    
    public  String getPathToSave(){
        String pathToSave = getPathToSaveR("-firmado");
        return pathToSave;
    }

    public  String getPathToSaveExtended() {
        String pathToExtend = getPathToSaveR("-sellado");
        return pathToExtend;
    }

    public  PasswordProtection getPin() {
        JPasswordField pinField = new JPasswordField(14);
        pinField.addHierarchyListener(new HierarchyListener() {
            public void hierarchyChanged(HierarchyEvent event) {
                final Component component = event.getComponent();
                if (component.isShowing() &&
                    (event.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED)
                    != 0) {
                    Window toplevel =
                        SwingUtilities.getWindowAncestor(component);
                    toplevel.addWindowFocusListener(new WindowAdapter() {
                        public void windowGainedFocus(WindowEvent event) {
                            component.requestFocus();
                        }
                    });
                }
            }
        });
        int action = JOptionPane.showConfirmDialog(null, pinField,
            "Ingresar PIN", JOptionPane.OK_CANCEL_OPTION,
            JOptionPane.QUESTION_MESSAGE);
        pinField.grabFocus();
        if (action == 0) {
            return new PasswordProtection(pinField.getPassword());
        } else {
            return new PasswordProtection(null);
        }
    }

    public  void showMessage(String message) {
        JOptionPane.showMessageDialog(null, new CopyableJLabel(message),
            "Mensaje de Firmador", JOptionPane.INFORMATION_MESSAGE);
    }

     private  String resizeImage(String imagePath, int width, int height){
                
                String imagePathResize = System.getProperty("user.home") + "/tmp.png";
            
                try{
                    if(imagePath != null){
                        Thumbnails.of(new File(imagePath))
                               .size(width,height)
                               .toFile(new File(imagePathResize));
                    }else{
                        showMessage("No se ha encontrado una imagen para la firma. \n Verificar que se ha seleccionado una");
                    }                
                  } catch (Exception e) {
                    Throwable exception = Throwables.getRootCause(e);
                    gui.showError(exception);   
                }
        return imagePathResize;
    }

}

class spinnerListener implements ChangeListener{
    private JSpinner spinner;
    private BufferedImage pageImage; 
    private PDFRenderer renderer;
    private JLabel imageLabel;
    private GUIInterface gui;
    public spinnerListener(JSpinner spinner, BufferedImage pageImage,PDFRenderer renderer, JLabel imageLabel,GUIInterface gui){
        this.spinner = spinner;
        this.pageImage = pageImage;
        this.renderer = renderer;
        this.imageLabel = imageLabel;
        this.gui = gui;
    }
    
    public void stateChanged(ChangeEvent e){
            int page = (int)spinner.getValue();
            if (page>=0){
                try{
                    pageImage = renderer.renderImage(page,1/1.25f);
                    imageLabel.setIcon(new ImageIcon(pageImage));
                }catch (Exception ex) {
                    ex.printStackTrace();
                    gui.showError(Throwables.getRootCause(ex));
                }
            }    
    }
}

class spinnerListenerCoord implements ChangeListener{
    private JSpinner spinner1;
    private JSpinner spinner2;
    private JLabel signLabel;
    
    public spinnerListenerCoord(JSpinner spinner1, JSpinner spinner2, JLabel signLabel){
        this.spinner1 = spinner1;
        this.spinner2 = spinner2;
        this.signLabel = signLabel;
    }
    
    public void stateChanged(ChangeEvent e){
        int coord = (int) spinner1.getValue();
        int coord2 = (int) spinner2.getValue();
        signLabel.setLocation(coord,coord2);
    }
}
