/* Firmador is a program to sign documents using AdES standards.

Copyright (C) 2020 Firmador authors.

This file is part of Firmador.

Firmador is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Firmador is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Firmador.  If not, see <http://www.gnu.org/licenses/>.  */

package app.firmador.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.Window;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.HierarchyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.URI;
import java.nio.file.Paths;
import java.security.KeyStore.PasswordProtection;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListSelectionModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.TransferHandler;
import javax.swing.ImageIcon;
import app.firmador.FirmadorPAdES;
import app.firmador.FirmadorXAdES;
import app.firmador.FirmadorOpenDocument;
import app.firmador.Report;
import app.firmador.Validator;
import app.firmador.gui.swing.CopyableJLabel;
import app.firmador.gui.swing.ScrollableJPanel;
import app.firmador.gui.GUISign;
import com.apple.eawt.Application;
import com.google.common.base.Throwables;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.FileDocument;
import eu.europa.esig.dss.model.MimeType;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.PDFRenderer;
import javax.swing.Icon;
import javax.imageio.ImageIO; 
import java.awt.Insets;

public class GUISwing implements GUIInterface {

    private static FileDialog loadDialog;
    private String documenttosign = null;
    private String documenttosave = null;
    private String lastDirectory = null;
    private String lastFile = null;
    private Image image = new ImageIcon(this.getClass().getClassLoader()
        .getResource("firmador.png")).getImage();
    private JTextField fileField;
    private JTabbedPane tabbedPane;
    private JLabel imageLabel;
    private JLabel signatureLabel;
    private JCheckBox signatureVisibleCheckBox;
    private CopyableJLabel reportLabel;
    private JLabel pageLabel;
    private JSpinner pageSpinner;
    private JButton signButton;
    private JButton extendButton;
    private BufferedImage pageImage;
    private PDDocument doc;
    private PDFRenderer renderer;

    public void loadGUI() {
        final JFrame frame = new JFrame("Firmador");
        frame.setIconImage(image.getScaledInstance(256, 256, Image.SCALE_SMOOTH));
        
        Vector filesSelected = new Vector(5,5);
        final DefaultListModel listModel = new DefaultListModel(); 
        final JList list = new JList(listModel);
        JScrollPane listScrollPane = new JScrollPane(list);
        list.setDragEnabled(true);
        list.setTransferHandler(new FileListTransferHandler(list,listModel));
        
        ListSelectionModel listSel = list.getSelectionModel();
        listSel.addListSelectionListener(    new ListSelectionHandler(filesSelected,listModel,this));
        JButton addB = new JButton();
        JButton removeB = new JButton();
        JButton signB = new JButton();
        JButton validate = new JButton();

        try{
            Image addI = ImageIO.read(this.getClass().getClassLoader()
            .getResource("add.png"));
            addB.setIcon(new ImageIcon(addI));

            Image removeI = ImageIO.read(this.getClass().getClassLoader()
            .getResource("remove.png"));
            removeB.setIcon(new ImageIcon(removeI));

            Image signI = ImageIO.read(this.getClass().getClassLoader()
            .getResource("sign.png"));
            signB.setIcon(new ImageIcon(signI));

            Image validateI = ImageIO.read(this.getClass().getClassLoader()
            .getResource("validate.png"));
            validate.setIcon(new ImageIcon(validateI));
        }catch (Exception ex) {
            System.out.println(ex);
        }
        
        addB.setMargin(new Insets(1,1,1,1));
        addB.setBorder(null);

        removeB.setMargin(new Insets(1,1,1,1));
        removeB.setBorder(null);

        signB.setMargin(new Insets(1,1,1,1));
        signB.setBorder(null);

        validate.setMargin(new Insets(1,1,1,1));
        validate.setBorder(null);

        addB.addActionListener(new FileOpener(list,listModel,frame));
        removeB.addActionListener(new Remover(list,listModel,filesSelected));
        signB.addActionListener(new Signer(list,listModel,filesSelected,this));
        validate.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event) {
                if(!listModel.isEmpty()){
                    File fileToValidate = (File)listModel.getElementAt((int)filesSelected.firstElement());
                        getValidator(fileToValidate);
                }
            }
        });

        JPanel ButtonPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        ButtonPanel.add(addB);
        ButtonPanel.add(removeB);
        ButtonPanel.add(signB); 
        ButtonPanel.add(validate);
        
        reportLabel = new CopyableJLabel();
        JPanel validatePanel = new ScrollableJPanel();
        validatePanel.add(reportLabel);
        JScrollPane validateScrollPane = new JScrollPane();
        validateScrollPane.setHorizontalScrollBarPolicy(
                ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        validateScrollPane.setBorder(null);
        validateScrollPane.setViewportView(validatePanel);
        JSplitPane splitHor = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,list,validateScrollPane);
        splitHor.setDividerLocation(300);
        Dimension minimumSize = new Dimension(100,50);
        list.setMinimumSize(minimumSize);
        validateScrollPane.setMinimumSize(minimumSize);
        splitHor.setPreferredSize(new Dimension(1000,650));
        frame.add(ButtonPanel,BorderLayout.NORTH);
        frame.add(splitHor,BorderLayout.AFTER_LAST_LINE);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.pack();
        frame.setLocationRelativeTo(null);
        if (documenttosign != null) {
            loadDocument(documenttosign);
        }
    }

    public void loadDocument(String fileName) {
        fileField.setText(fileName);
        signButton.setEnabled(true);
        DSSDocument mimeDocument = new FileDocument(fileName);
        MimeType mimeType = mimeDocument.getMimeType();
        try {
            if (doc != null) {
                doc.close();
            }
            if (mimeType == MimeType.PDF) {
                doc = PDDocument.load(new File(fileName));
                int pages = doc.getNumberOfPages();
                renderer = new PDFRenderer(doc);
                pageLabel.setVisible(true);
                pageSpinner.setVisible(true);
                signatureVisibleCheckBox.setVisible(true);
                if (pages > 0) {
                    pageImage = renderer.renderImage(0, 1 / 2.5f);
                    SpinnerNumberModel model =
                        ((SpinnerNumberModel)pageSpinner.getModel());
                    model.setMinimum(1);
                    model.setMaximum(pages);
                    pageLabel.setEnabled(true);
                    pageSpinner.setEnabled(true);
                    signatureVisibleCheckBox.setEnabled(true);
                    pageSpinner.setValue(1);
                }
                imageLabel.setBorder(new LineBorder(Color.BLACK));
                imageLabel.setIcon(new ImageIcon(pageImage));
                imageLabel.setVisible(true);
            }
            else if (mimeType == MimeType.ODG || mimeType == MimeType.ODP
                || mimeType == MimeType.ODS || mimeType == MimeType.ODT
                || mimeType == MimeType.XML) {
                imageLabel.setVisible(false);
                pageLabel.setVisible(false);
                pageSpinner.setVisible(false);
                signatureVisibleCheckBox.setVisible(false);
            }
        } catch (Exception e) {
            showError(Throwables.getRootCause(e));
        }

    }

    public String getDocumentToSign() {
        return fileField.getText();
    }

    public String getPathToSave() {
        if (documenttosave != null) {
            return documenttosave;
        }
        String pathToSave = showSaveDialog("-firmado");

        return pathToSave;
    }

    public String getPathToSaveExtended() {
        String pathToExtend = showSaveDialog("-sellado");

        return pathToExtend;
    }

    public String showSaveDialog(String suffix) {
        String fileName = null;
        FileDialog saveDialog = null;
        saveDialog = new FileDialog(saveDialog,
            "Guardar documento", FileDialog.SAVE);
        saveDialog.setDirectory(lastDirectory);

        String dotExtension = "";
        int lastDot = lastFile.lastIndexOf(".");
        if (lastDot >= 0) {
            dotExtension = lastFile.substring(lastDot);
        }
        saveDialog.setFile(lastFile.substring(0,
            lastFile.lastIndexOf(".")) + suffix + dotExtension);
        saveDialog.setFilenameFilter(loadDialog.getFilenameFilter());
        saveDialog.setLocationRelativeTo(null);
        saveDialog.setVisible(true);
        saveDialog.dispose();
        if (saveDialog.getFile() != null) {
            fileName = saveDialog.getDirectory() + saveDialog.getFile();
            lastDirectory = saveDialog.getDirectory();
            lastFile = saveDialog.getFile();
        }

        return fileName;
    }

    public PasswordProtection getPin() {
        JPasswordField pinField = new JPasswordField(14);
        pinField.addHierarchyListener(new HierarchyListener() {
            public void hierarchyChanged(HierarchyEvent event) {
                final Component component = event.getComponent();
                if (component.isShowing() &&
                    (event.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED)
                    != 0) {
                    Window toplevel =
                        SwingUtilities.getWindowAncestor(component);
                    toplevel.addWindowFocusListener(new WindowAdapter() {
                        public void windowGainedFocus(WindowEvent event) {
                            component.requestFocus();
                        }
                    });
                }
            }
        });
        int action = JOptionPane.showConfirmDialog(null, pinField,
            "Ingresar PIN", JOptionPane.OK_CANCEL_OPTION,
            JOptionPane.QUESTION_MESSAGE);
        pinField.grabFocus();
        if (action == 0) {
            return new PasswordProtection(pinField.getPassword());
        } else {
            return new PasswordProtection(null);
        }
    }

    public void setArgs(String[] args) {
        List<String> arguments = new ArrayList<String>();

        for (String params : args) {
            if (!params.startsWith("-")) {
                arguments.add(params);
            }
        }
        if (arguments.size() > 1) {
            documenttosign = Paths.get(arguments.get(0)).toAbsolutePath()
                .toString();
        }
        if (arguments.size() > 2) {
            documenttosave = Paths.get(arguments.get(1)).toAbsolutePath()
                .toString();
        }
    }

    public void showError(Throwable error) {
        String message = error.getLocalizedMessage();
        int messageType = JOptionPane.ERROR_MESSAGE;
        String className = error.getClass().getName();

        switch (className) {
            case "java.lang.NoSuchMethodError":
                message = "Esta aplicación es actualmente incompatible con " +
                    "versiones superiores a Java 8.<br>" +
                    "Este inconveniente se corregirá en próximas versiones. " +
                    "Disculpe las molestias.";
                break;
            case "java.security.ProviderException":
                message = "No se ha encontrado la librería de Firma Digital " +
                    "en el sistema.<br>" +
                    "¿Están instalados los controladores?";
                break;
            case "java.security.NoSuchAlgorithmException":
                message = "No se ha encontrado ninguna tarjeta conectada." +
                    "<br>Asegúrese de que la tarjeta y el lector están " +
                    "conectados de forma correcta.";
                break;
            case "sun.security.pkcs11.wrapper.PKCS11Exception":
                switch (message) {
                case "CKR_GENERAL_ERROR":
                    message = "No se ha podido contactar con el servicio " +
                        "del lector de tarjetas.<br>" +
                        "¿Está correctamente instalado o configurado?";
                    break;
                case "CKR_SLOT_ID_INVALID":
                    message = "No se ha podido encontrar ningún lector " +
                    "conectado o el controlador del lector no está instalado.";
                    break;
                case "CKR_PIN_INCORRECT":
                    messageType = JOptionPane.WARNING_MESSAGE;
                    message = "¡PIN INCORRECTO!<br><br>" +
                        "ADVERTENCIA: si se ingresa un PIN incorrecto " +
                        "varias veces sin acertar,<br>" +
                        "el dispositivo de firma se bloqueará.";
                    break;
                case "CKR_PIN_LOCKED":
                    message = "PIN BLOQUEADO<br><br>" +
                        "Lo sentimos, el dispositivo de firma no se puede " +
                        "utilizar porque está bloqueado.<br>" +
                        "Contacte con su proveedor para desbloquearlo.";
                    break;
                default:
                    break;
                }
                break;
            case "java.io.IOException":
                if (message.contains("asepkcs") ||
                    message.contains("libASEP11")) {
                    message = "No se ha encontrado la librería de Firma " +
                        "Digital en el sistema.<br>" +
                        "¿Están instalados los controladores?";
                    break;
                }
            default:
                error.printStackTrace();
                message = "Error: " + className + "<br>" +
                    "Detalle: " + message + "<br>" +
                    "Agradecemos que comunique este mensaje de error a los " +
                    "autores del programa<br>" +
                    "para detallar mejor el posible motivo de este error " +
                    "en próximas versiones.";
                break;
        }

        JOptionPane.showMessageDialog(null, new CopyableJLabel(message),
            "Mensaje de Firmador", messageType);

        if (messageType == JOptionPane.ERROR_MESSAGE) {
            System.exit(0);
        }
    }

    public void showMessage(String message) {
        JOptionPane.showMessageDialog(null, new CopyableJLabel(message),
            "Mensaje de Firmador", JOptionPane.INFORMATION_MESSAGE);
    }

    public int getSelection(String[] options) {
        int dev = 0;

        if (options == null || options.length == 0) {
            String message = "No se ha encontrado ninguna tarjeta " +
                "conectada.<br>Asegúrese de que la tarjeta y el lector " +
                "están conectados de forma correcta.";
            JOptionPane.showMessageDialog(null, new CopyableJLabel(message),
                "Error al firmar", JOptionPane.ERROR_MESSAGE);
            return -1;
        }
        String input = JOptionPane.showInputDialog(null, "Propietario: ",
            "Seleccione el dispositivo para firmar",
            JOptionPane.QUESTION_MESSAGE, null, options, options[0])
            .toString();

        if (input == null) {
            return -1;
        }
        for (int x = 0; x < options.length; x++) {
            if (input.equals(options[x])) {
                dev = x;
                x = options.length;
            }
        }

        return dev;
    }

    public void getValidator(File fileName){
        DSSDocument mimeDocument = new FileDocument(fileName);
        MimeType mimeType = mimeDocument.getMimeType();
        Validator validator = null;
        try {
            validator = new Validator(fileName);
        }catch (Exception e) {
            if (mimeType == MimeType.ODG || mimeType == MimeType.ODP
                || mimeType == MimeType.ODS || mimeType == MimeType.ODT) {
                // Workaround for DSS 5.6 not recognizing unsigned ODF files
            } else {
                e.printStackTrace();
                reportLabel.setText("Error al validar documento. " +
                    "Agradeceríamos que informara sobre este inconveniente " +
                    "a los desarrolladores de la aplicación para repararlo.");
            }
            reportLabel.setText("");
            extendButton.setEnabled(false);
            tabbedPane.setSelectedIndex(0);
        }

        if (validator != null) {
            try {
                Report report = new Report(validator.getReports());
                reportLabel.setText(report.getReport());
            } catch (Exception e) {
                e.printStackTrace();
                reportLabel.setText("Error al generar reporte. " +
                    "Agradeceríamos que informara sobre este inconveniente " +
                    "a los desarrolladores de la aplicación para repararlo.");
            }
        }
    }
}

class Signer implements ActionListener{
    private JList list;
    private DefaultListModel listModel;
    private Vector filesSelected;
    protected GUIInterface gui;
    
    public Signer(JList list,DefaultListModel listModel, Vector filesSelected, GUIInterface gui){
        this.list = list;
        this.listModel = listModel;
        this.filesSelected = filesSelected;
        this.gui = gui;
    }

    public void actionPerformed(ActionEvent event) {
        if (filesSelected.size()!= 0){

        GUISign signPanel = new GUISign((File)listModel.getElementAt((int)filesSelected.firstElement()),gui);
        JFrame signFrame = signPanel.createSignGUI();
        
    }}
    
}


class Remover implements ActionListener{
    private JList list;
    private DefaultListModel listModel;
    private Vector filesSelected;

    
    public Remover(JList list,DefaultListModel listModel, Vector filesSelected) {
        this.list = list;
        this.listModel = listModel;
        this.filesSelected = filesSelected;
    }
    
    public void actionPerformed(ActionEvent event) {
        int removeList [] =  new int[filesSelected.size()];
        for (int i =0; i<removeList.length;i++){
            removeList[i] = (int) filesSelected.get(i);
        }
        
        for (int i =0; i<removeList.length;i++){
            listModel.remove(removeList[i]-i);
            System.out.printf("Removing item from JList: %d", removeList[i]-i);
        }
        System.out.println("Done Removing");
    }
    
}
class FileOpener implements ActionListener{
    private JList list;
    private DefaultListModel listModel;
    private JFrame frame;

    
    public FileOpener(JList list,DefaultListModel listModel,JFrame frame) {
        this.list = list;
        this.listModel = listModel;
        this.frame = frame; 
    }
    
    public void actionPerformed(ActionEvent event) {
        FileDialog loadDialog = new FileDialog(frame,"Seleccionar documento a firmar");
        loadDialog.setMultipleMode(true);
        loadDialog.setFilenameFilter(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".odg")
                || name.toLowerCase().endsWith(".odp")
                || name.toLowerCase().endsWith(".ods")
                || name.toLowerCase().endsWith(".odt")
                || name.toLowerCase().endsWith(".pdf")
                || name.toLowerCase().endsWith(".xml");
            }
        });
        loadDialog.setFile("*.odg;*.odp;*.ods;*.odt;*.pdf;*.xml");
        loadDialog.setLocationRelativeTo(null);
        loadDialog.setVisible(true);
        loadDialog.dispose();
        if (loadDialog.getFiles() != null) {
            for (int i = 0; i < loadDialog.getFiles().length; i++) {
                listModel.addElement(loadDialog.getFiles()[i]);
                System.out.printf("The number of elements in listModel is: %d \n", listModel.getSize());
            }      
        }
    }
}


class ListSelectionHandler implements ListSelectionListener{
    private Vector filesSelected;
    private DefaultListModel listModel;
    private GUISwing gui;
    public ListSelectionHandler(Vector filesSelected, DefaultListModel listModel,GUISwing gui){
        this.filesSelected = filesSelected;
        this.listModel = listModel;
        this.gui = gui;
    }
    public void valueChanged(ListSelectionEvent e) {
            filesSelected.clear();
        ListSelectionModel lsm = (ListSelectionModel)e.getSource();
            int minIndex = lsm.getMinSelectionIndex();

            if (lsm.isSelectionEmpty()) {
            } else {
                int maxIndex = lsm.getMaxSelectionIndex();
                for (int i = minIndex; i <= maxIndex; i++) {
                    if  (lsm.isSelectedIndex(i)) {
                            filesSelected.addElement(i);    
                    }
                }
            }
            //if(minIndex >= 0){
             //   gui.getValidator((File)listModel.getElementAt(minIndex));
            //}

            System.out.print("[");
            for (int i = 0; i<filesSelected.size();i++){
                System.out.printf(" %d, ",filesSelected.get(i));
            }
            System.out.print("]");
            System.out.printf("Number of selected items are: %d \n", filesSelected.size());
        
    }
}



class FileListTransferHandler extends TransferHandler {
    private DefaultListModel listModel;
    private JList list;

   public FileListTransferHandler(JList list, DefaultListModel listModel) {
      this.listModel = listModel;
      this.list = list;
   }

   public boolean canImport(TransferHandler.TransferSupport ts) {
    
   
    return ts.isDataFlavorSupported(DataFlavor.javaFileListFlavor);
   
    }

   public boolean importData(TransferHandler.TransferSupport ts) {
      try {
         @SuppressWarnings("rawtypes")
         java.util.List data = (java.util.List) ts.getTransferable().getTransferData(
               DataFlavor.javaFileListFlavor);
         if (data.size() < 1) {
            return false;
         }
         System.out.printf("The size of data is: %d \n", data.size());
         for (Object item : data) {
            File file = (File) item;
            String name = file.getName();
            if(name.toLowerCase().endsWith(".odg")
            || name.toLowerCase().endsWith(".odp")
            || name.toLowerCase().endsWith(".ods")
            || name.toLowerCase().endsWith(".odt")
            || name.toLowerCase().endsWith(".pdf")
            || name.toLowerCase().endsWith(".xml")){
                listModel.addElement(file);
            }
        }
        System.out.printf("The number of elements in listModel is: %d \n", listModel.getSize());
        
         return true;

      } catch (UnsupportedFlavorException e) {
         return false;
      } catch (IOException e) {
         return false;
      }
   }
}

