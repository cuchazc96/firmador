/* Firmador is a program to sign documents using AdES standards.

Copyright (C) 2020 Firmador authors.

This file is part of Firmador.

Firmador is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Firmador is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Firmador.  If not, see <http://www.gnu.org/licenses/>.  */

package app.firmador;

import java.awt.Color;
import java.awt.Font;
import java.awt.font.TextAttribute;
import java.security.KeyStore.PasswordProtection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import javax.security.auth.x500.X500Principal;
import app.firmador.gui.GUIInterface;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.File;
import com.google.common.base.Throwables;

import eu.europa.esig.dss.enumerations.DigestAlgorithm;
import eu.europa.esig.dss.enumerations.SignatureLevel;
import eu.europa.esig.dss.model.DSSDocument;
import eu.europa.esig.dss.model.DSSException;
import eu.europa.esig.dss.model.SignatureValue;
import eu.europa.esig.dss.model.ToBeSigned;
import eu.europa.esig.dss.model.x509.CertificateToken;
import eu.europa.esig.dss.model.x509.X500PrincipalHelper;
import eu.europa.esig.dss.pades.DSSJavaFont;
import eu.europa.esig.dss.pades.PAdESSignatureParameters;
import eu.europa.esig.dss.pades.SignatureImageParameters;
import eu.europa.esig.dss.pades.SignatureImageTextParameters;
import eu.europa.esig.dss.pades.signature.PAdESService;
import eu.europa.esig.dss.pdf.pdfbox.PdfBoxNativeObjectFactory;
import eu.europa.esig.dss.service.tsp.OnlineTSPSource;
import eu.europa.esig.dss.spi.DSSASN1Utils;
import eu.europa.esig.dss.token.DSSPrivateKeyEntry;
import eu.europa.esig.dss.token.SignatureTokenConnection;
import eu.europa.esig.dss.validation.CertificateVerifier;
import eu.europa.esig.dss.model.InMemoryDocument;
import org.bouncycastle.asn1.x500.style.BCStyle;

public class FirmadorPAdES extends CRSigner {

    private int page, x, y;
    private String text;
    private int size,format;
    private String imagePath;
    private String imagePathResize; 
    private boolean square, useDefault, bold, italics;
    
    PAdESSignatureParameters parameters;
    private boolean visible_signature = true;

    public FirmadorPAdES(GUIInterface gui) {
        super(gui);
    }

    public boolean isVisible_signature() {
        return visible_signature;
    }


    public void setVisible_signature(boolean visible_signature) {
        this.visible_signature = visible_signature;
    }

    public void addVisibleSignature(int page, int x, int y,String text, int size, int format, String imagePath,boolean square, boolean useDefault, boolean bold, boolean italics) {
        this.square = square;
        this.page = page;
        this.text = text;
        this.x = x;
        this.y = y;
        this.size = size;
        this.format = format;
        this.imagePath = imagePath;
        this.bold= bold;
        this.italics = italics;
        this.useDefault = useDefault;

        System.out.printf("Path de la imagen es: %s", imagePath);
        if (imagePath != null){
            String dotExtension = "";
            int lastDot = imagePath.lastIndexOf(".");
            if (lastDot >= 0) {
                dotExtension = imagePath.substring(lastDot);
            }
            imagePathResize = imagePath.substring(0,
            imagePath.lastIndexOf(".")) + "resize" + dotExtension;
        }
    }

    private void appendVisibleSignature(CertificateToken certificate, Date date) {
        
        SignatureImageParameters imageParameters =
                new SignatureImageParameters();
                imageParameters.setxAxis(x);
                imageParameters.setyAxis(y);
                
        if(format != 1)
        {
            SignatureImageTextParameters textParameters = new SignatureImageTextParameters();
            String signText = "";
            if(!useDefault){
                String cn = DSSASN1Utils.getSubjectCommonName(certificate);
                X500PrincipalHelper subject = certificate.getSubject();
                String o = DSSASN1Utils.extractAttributeFromX500Principal(BCStyle.O, subject);
                String sn = DSSASN1Utils.extractAttributeFromX500Principal(BCStyle.SERIALNUMBER, subject);
                String fecha = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss Z").format(date);
                signText = "Firmado digitalmente por " + cn + "\n" + "Fecha declarada: " + fecha + "\n";
            }
            signText += text; 
            
            int textFont = Font.PLAIN;
            int baseSize = 6;
            if (bold){
                if(italics){
                        textFont = Font.BOLD|Font.ITALIC ;
                }else{
                    textFont = Font.BOLD;
                }
            }else if(italics){
                    textFont = Font.ITALIC;
            }
            int numLines = 0;
            String newsignText ="";
            if (format == 2)
            {
                newsignText = resizeText(signText,36,1.33);
                numLines = getNumberOfLines(newsignText);
                if (numLines > 5 )
                {
                    baseSize--;
                    newsignText = resizeText(signText,42,1.32); //5
                    numLines = getNumberOfLines(newsignText);
                    if (numLines >= 7){
                        baseSize--; //4
                        newsignText = resizeText(signText,54,1.32); 
                        numLines = getNumberOfLines(newsignText);
                        if (numLines > 7){
                            baseSize--; //3
                            newsignText = resizeText(signText,50,1.32);    
                        }
                    }
                }  
            }else{
                if (square){
                    newsignText = resizeText(signText,28,1.32);
                }else{
                    newsignText = resizeText(signText,18,1.32);  
                }
                numLines = getNumberOfLines(newsignText);
                    
                if (numLines > 5)
                {   if (square){
                        newsignText = resizeText(signText,28,1.32);
                    }else{
                        newsignText = resizeText(signText,24,1.32);  
                    }
                    numLines = getNumberOfLines(newsignText);
                    if (numLines==5 || numLines==6){                    
                        baseSize = baseSize - 2;
                    }else{
                        baseSize = baseSize - 2;
                        if (square){
                            newsignText = resizeText(signText,38,1.32);
                        }else{
                            newsignText = resizeText(signText,34,1.32);  
                        }
                        numLines = getNumberOfLines(newsignText);
                        if (numLines < 7){
                            baseSize--;
                        }else{
                            if (square){
                                newsignText = resizeText(signText,48,1.32);
                            }else{
                                newsignText = resizeText(signText,44,1.32);  
                            }
                            baseSize--;
                        }
                    }
                }
            }
                System.out.printf("Final Size: %d \n", baseSize);
                System.out.printf("Final resized text: %s \n ", newsignText);
                textParameters.setFont(
                    new DSSJavaFont(new Font(Font.SANS_SERIF, textFont, baseSize)));                 
              textParameters.setText(newsignText);
              textParameters.setBackgroundColor(new Color(255, 255, 255, 0));
              imageParameters.setTextParameters(textParameters);   
        }
        imageParameters.setPage(page);   
        if(format >= 0 && format < 2){
                try{
                    InputStream image = new FileInputStream(imagePath);
                    System.out.println("Creando input stream");
                    DSSDocument imageDoc = new InMemoryDocument(image);
                    imageParameters.setImage(imageDoc);
                  } catch (Exception e) {
                    Throwable exception = Throwables.getRootCause(e);
                    gui.showError(exception);   
                }
        }
          imageParameters.setZoom(size);
          imageParameters.setBackgroundColor(Color.WHITE);
          parameters.setImageParameters(imageParameters);
    }

    public String resizeText(String textToResize, int numChar, double conversion){
        String []linesString = textToResize.split("\n");
        String resizedText = "";

        List<String> newlinesString = new ArrayList<String>(); 


        for (int i=0; i<linesString.length;i++){
            String textToEvaluate = linesString[i];
            int lower = 0;
            int upper = 0;
            for(int k=0; k<textToEvaluate.length(); k++){
                char character = textToEvaluate.charAt(k);
                if(Character.isUpperCase(character)){
                    upper++;
                }else{
                    lower++;
                }
            }

            int lineLength = (int)Math.ceil(upper * conversion) + lower;

            if (lineLength > numChar){
                String printText = "The text to evaluate: " + textToEvaluate + "with lenght: " +  lineLength + "is bigger than: " + numChar; 
                System.out.println(printText);
                String []spaceString = textToEvaluate.split(" ");

                    String createdString="";
                    int index = 0;
                    for (int j=0; j<spaceString.length; j++){
                        lower = 0;
                        upper = 0;
                        for(int w=0; w<spaceString[j].length(); w++){
                            char character = spaceString[j].charAt(w);
                            if(Character.isUpperCase(character)){
                                upper++;
                            }else{
                                lower++;
                            }
                        }
                        int wordLength = (int)Math.ceil(upper*conversion) + lower;

                        if (index + wordLength < numChar-1){
                            createdString += " "+spaceString[j];
                            index += wordLength + 1;
                        }else{
                            newlinesString.add(createdString);
                            createdString = spaceString[j];
                            index = wordLength + 1;    
                        }
                        if (j == spaceString.length - 1){
                            newlinesString.add(createdString);    
                        }  
                    }
            }else{
                newlinesString.add(textToEvaluate);
            }
        }
        System.out.println("New Strings:");
        for (int i=0; i<newlinesString.size(); i++){
            resizedText += newlinesString.get(i) + "\n";            
            System.out.println(newlinesString.get(i));
        }
        return resizedText;
    }

    public int getNumberOfLines(String textToResize){
        String []linesString = textToResize.split("\n");
        System.out.printf("Number of lines: %d \n", linesString.length);
        return linesString.length;
    }

    
    public DSSDocument sign(DSSDocument toSignDocument,
        PasswordProtection pin) {

        CertificateVerifier verifier = this.getCertificateVerifier();
        verifier.setCheckRevocationForUntrustedChains(true);

        PAdESService service = new PAdESService(verifier);
        service.setPdfObjFactory(new PdfBoxNativeObjectFactory());
        parameters = new PAdESSignatureParameters();

        SignatureValue signatureValue = null;

        DSSDocument signedDocument = null;

        try {
            System.out.println("Creando token");
            SignatureTokenConnection token = getSignatureConnection(pin);

            System.out.println("Creando privateKey");
            DSSPrivateKeyEntry privateKey = getPrivateKey(token);

            System.out.println("Creando certificado");
            CertificateToken certificate = privateKey.getCertificate();
            parameters.setSignatureLevel(SignatureLevel.PAdES_BASELINE_LT);
            parameters.setContentSize(13312);
            parameters.setDigestAlgorithm(DigestAlgorithm.SHA256);
            parameters.setSigningCertificate(certificate);


            System.out.println("Creando certificateChain");
            List<CertificateToken> certificateChain = getCertificateChain(
                verifier, parameters);
            
            System.out.println("setteando certificateChain a parameters");
            
            parameters.setCertificateChain(certificateChain);
            
            System.out.println("Creando OnlineTSPSource");
            
            OnlineTSPSource onlineTSPSource = new OnlineTSPSource(TSA_URL);
            
            System.out.println("Setteando OnlineTSPSource");
            service.setTspSource(onlineTSPSource);
            Date date = new Date();

            if(visible_signature) {
                System.out.println("AppendVisibleSignature");
                appendVisibleSignature(certificate, date);
            }
            System.out.println("SetSigningDate");
            parameters.bLevel().setSigningDate(date);
            
            System.out.println("getDataToSign");
            ToBeSigned dataToSign = service.getDataToSign(toSignDocument,
                parameters);

            System.out.println("Token sign");
            signatureValue = token.sign(dataToSign,
                parameters.getDigestAlgorithm(), privateKey);
        
            } catch (DSSException|Error e) {
            Throwable exception = Throwables.getRootCause(e);
            gui.showError(exception);   
        }

        try {
            signedDocument = service.signDocument(toSignDocument, parameters,
                signatureValue);
        } catch (Exception e) {
            e.printStackTrace();
            gui.showMessage(
                "Aviso: no se ha podido agregar el sello de tiempo y la " +
                "información de revocación porque es posible<br>" +
                "que haya problemas de conexión a Internet o con los " +
                "servidores del sistema de Firma Digital.<br>" +
                "Detalle del error: " + Throwables.getRootCause(e) + "<br>" +
                "<br>" +
                "Se ha agregado una firma básica solamente. No obstante, si " +
                "el sello de tiempo resultara importante<br>" +
                "para este documento, debería agregarse lo antes posible " +
                "antes de enviarlo al destinatario.<br>" +
                "<br>" +
                "Si lo prefiere, puede cancelar el guardado del documento " +
                "firmado e intentar firmarlo más tarde.<br>");

            parameters.setSignatureLevel(SignatureLevel.PAdES_BASELINE_B);
            try {
                signedDocument = service.signDocument(toSignDocument,
                    parameters, signatureValue);
            } catch (Exception ex) {
                e.printStackTrace();
                gui.showError(Throwables.getRootCause(e));
            }
        }

        return signedDocument;
    }

    public DSSDocument extend(DSSDocument document) {
        PAdESSignatureParameters parameters =
            new PAdESSignatureParameters();
        parameters.setSignatureLevel(SignatureLevel.PAdES_BASELINE_LTA);
        parameters.setContentSize(3072);

        CertificateVerifier verifier = this.getCertificateVerifier();
        verifier.setCheckRevocationForUntrustedChains(true);

        PAdESService service = new PAdESService(verifier);

        OnlineTSPSource onlineTSPSource = new OnlineTSPSource(TSA_URL);
        service.setTspSource(onlineTSPSource);

        DSSDocument extendedDocument = null;
        try {
            extendedDocument = service.extendDocument(document,
                parameters);
        } catch (Exception e) {
            e.printStackTrace();
            gui.showMessage(
                "Aviso: no se ha podido agregar el sello de tiempo y la " +
                "información de revocación porque es posible<br>" +
                "que haya problemas de conexión a Internet o con los " +
                "servidores del sistema de Firma Digital.<br>" +
                "Detalle del error: " + Throwables.getRootCause(e) + "<br>" +
                "<br>" +
                "Inténtelo de nuevo más tarde. Si el problema persiste, " +
                "compruebe su conexión o verifique<br>" +
                "que no se trata de un problema de los servidores de Firma " +
                "Digital o de un error de este programa.<br>");
        }

        return extendedDocument;
    }

}
